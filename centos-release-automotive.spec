%global distro  CentOS AutoSD
%global major   9
%global minor   0

Name:       centos-release-automotive
Version:    %{major}.%{minor}
Release:    4%{?dist}
License:    GPLv2
URL:        http://wiki.centos.org/SpecialInterestGroup/Automotive
Summary:    CentOS Automotive SIG main repo configs
Source0:    RPM-GPG-KEY-CentOS-SIG-Automotive
Source1:    CentOS-Stream-Automotive.repo
Source2:    CentOS-Stream-Automotive-Experimental.repo

Source300:    90-default.preset
Source301:    99-default-disable.preset

BuildArch:  noarch

Conflicts:  centos-stream-release
Provides:   centos-release
Provides:   centos-stream-release
Provides:   system-release
Provides:   system-release(releasever) = %{major}

# We still want to have the centos-stream repos and gpg keys
Requires:   centos-gpg-keys
Requires:   centos-stream-repos

%description
Configs for the CentOS Automotive SIG main package repository and release
files.

%package experimental
Summary: CentOS Automotive SIG experimental repo configs
Requires: %{name} = %{version}-%{release}

%description experimental
Configs for the CentOS Automotive SIG experimental package repository.

%prep
# Nothing to do

%install
# Install the release files

# create skeleton
mkdir -p %{buildroot}/etc
mkdir -p %{buildroot}%{_prefix}/lib


# create /etc/system-release and /etc/redhat-release
install -d -m 0755 %{buildroot}%{_sysconfdir}
echo "%{distro} release %{major}" > %{buildroot}%{_sysconfdir}/centos-release
ln -s centos-release %{buildroot}%{_sysconfdir}/system-release
ln -s centos-release %{buildroot}%{_sysconfdir}/redhat-release

# Create the os-release file
install -d -m 0755 %{buildroot}%{_prefix}/lib
cat > %{buildroot}%{_prefix}/lib/os-release << EOF
NAME="%{distro}"
VERSION="%{major}"
ID="AutoSD"
ID_LIKE="rhel fedora"
VERSION_ID="%{major}"
PLATFORM_ID="platform:el%{major}"
PRETTY_NAME="%{distro} %{major}"
ANSI_COLOR="0;31"
LOGO="fedora-logo-icon"
CPE_NAME="cpe:/o:centos:autosd:%{major}"
HOME_URL="https://centos.org/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_SUPPORT_PRODUCT="Red Hat Enterprise Linux %{major}"
REDHAT_SUPPORT_PRODUCT_VERSION="%{distro}"
EOF

# Create the symlink for /etc/os-release
ln -s ../usr/lib/os-release %{buildroot}%{_sysconfdir}/os-release

# write cpe to /etc/system/release-cpe
echo "cpe:/o:centos:autosd:%{major}" > %{buildroot}%{_sysconfdir}/system-release-cpe

# create /etc/issue, /etc/issue.net and /etc/issue.d
echo '\S' > %{buildroot}%{_sysconfdir}/issue
echo 'Kernel \r on an \m' >> %{buildroot}%{_sysconfdir}/issue
cp %{buildroot}%{_sysconfdir}/issue{,.net}
echo >> %{buildroot}%{_sysconfdir}/issue
mkdir -p %{buildroot}%{_sysconfdir}/issue.d


# Install the repo files
install -d %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -p -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -d %{buildroot}%{_sysconfdir}/yum.repos.d
install -p -m 644 %{SOURCE1} %{SOURCE2} \
  %{buildroot}%{_sysconfdir}/yum.repos.d

# copy systemd presets
install -d -m 0755 %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -p -m 0644 %{SOURCE300} %{buildroot}%{_prefix}/lib/systemd/system-preset/

# installing the same file for both system and user presets to set the same behavior for both
install -d -m 0755 %{buildroot}%{_prefix}/lib/systemd/user-preset/
install -p -m 0644 %{SOURCE301} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -p -m 0644 %{SOURCE301} %{buildroot}%{_prefix}/lib/systemd/user-preset/

%files
%defattr(-,root,root)
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-Automotive
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-Stream-Automotive.repo
%{_sysconfdir}/redhat-release
%{_sysconfdir}/system-release
%{_sysconfdir}/centos-release
%config(noreplace) %{_sysconfdir}/os-release
%config %{_sysconfdir}/system-release-cpe
%config(noreplace) %{_sysconfdir}/issue
%config(noreplace) %{_sysconfdir}/issue.net
%{_prefix}/lib/os-release
%{_prefix}/lib/systemd/system-preset/*
%{_prefix}/lib/systemd/user-preset/*

%files experimental
%defattr(-,root,root)
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-Automotive
%config(noreplace) %{_sysconfdir}/yum.repos.d/CentOS-Stream-Automotive-Experimental.repo


%changelog
* Wed Jan 04 2023 Michael Ho <micho@redhat.com> - 9-4
- Add default .preset files

* Fri Oct 14 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 9-3
- Mark the repos to be skipped if unavailable

* Thu Oct 13 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 9-2
- Add a dependency to centos-stream-repos and centos-gpg-keys

* Tue Sep 06 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 9-1
- Initial package for CentOS Stream 9
